using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using LiteNetLib;
using UnityEngine;
using UnityEngine.UI;

public class Client : MonoBehaviour, INetEventListener
{
    [SerializeField] private Button _sendButton;
    private EventBasedNetListener _listener;
    private NetManager _client;
    
    void Start()
    {
        _listener = new EventBasedNetListener();
        _client = new NetManager(_listener);
        _client.UpdateTime = 15;
        _client.Start();
        _client.Connect("localhost" /* host ip or name */, 9050 /* port */, "SomeConnectionKey" /* text key or NetDataWriter */);
    }

    private void Update()
    {
        _client.PollEvents();
    }

    private void OnEnable()
    {
        _sendButton.onClick.AddListener(ServerPingPong);
    }

    private void OnDisable()
    {
        _sendButton.onClick.RemoveListener(ServerPingPong);
    }

    private void ServerPingPong()
    {
        
    }

    public void OnPeerConnected(NetPeer peer)
    {
        
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
    {
        
    }

    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        Console.WriteLine("We got: {0}", reader.GetString(100 /* max length of string */));
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
    {
        
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        
    }

    public void OnConnectionRequest(ConnectionRequest request)
    {
        
    }
}
