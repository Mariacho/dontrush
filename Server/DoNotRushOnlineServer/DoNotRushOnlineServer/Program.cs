﻿using System;
using DoNotRushOnlineServer.Network.Server;
using LiteNetLib;

namespace DoNotRushOnlineServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new ServerExample();
            server.Start();
        }
    }
}